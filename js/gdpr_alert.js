(function ($, Drupal, cookies) {
  Drupal.behaviors.gdpr_alert = {
    attach: function (context, drupalSettings) {

      if (drupalSettings.gdpr_alert && drupalSettings.gdpr_alert.dismissedKey
        && drupalSettings.gdpr_alert.cookieExpiration) {

        // Since the key is updated every time the configuration form is saved,
        // we can ensure users don't miss newly added or changed alerts.
        var key = drupalSettings.gdpr_alert.dismissedKey;

        // Only show the alert if dismiss button has not been clicked. The
        // element is hidden by default in order to prevent it from momentarily
        // flickering onscreen.
        if (cookies.get('Drupal.visitor.gdpr_alert_dismissed') !== key) {
          $('.c-gdpr-alert', context).show();
        }

        // Set the cookie value when dismiss button is clicked.
        $(once('gdpr_alert', '.c-gdpr-alert .c-gdpr-alert__close', context)).on('click', function (e) {

          // Do not perform default action.
          e.preventDefault();

          var options = {};
          var expiration = drupalSettings.gdpr_alert.cookieExpiration;

          // If the expiration value is "default", we dont need to set the expires property
          // as $.cookie will default to a session based cookie.
          if (expiration !== 'default') {
            options.expires = expiration;
          }
          options.path = drupalSettings.path.baseUrl;

          // Set cookie to the current key.
          cookies.set('Drupal.visitor.gdpr_alert_dismissed', key, options);

          // Remove alert
          $(this).parent().slideUp('normal', function () {
            $(this).remove();
          });
        });
      }
    }
  }
})(jQuery, Drupal, window.Cookies);
