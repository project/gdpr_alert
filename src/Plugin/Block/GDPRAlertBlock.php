<?php

namespace Drupal\gdpr_alert\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Language\LanguageManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * GDPR Alert Block.
 *
 * @Block(
 *   id          = "gdpr_alert",
 *   admin_label = @Translation("GDPR Alert Bar"),
 *   category    = @Translation("GDPR Alert"),
 * )
 */
class GDPRAlertBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The admin context.
   *
   * @var \Drupal\Core\Routing\AdminContext
   */
  protected $adminContext;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManager
   */
  protected $languageManager;

  /**
   * The current language id.
   *
   * @var string
   */
  protected $language;

  /**
   * The state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * GDPRAlertBlock constructor.
   *
   * @param array $configuration
   *   The configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Routing\AdminContext $admin_context
   *   The admin context.
   * @param \Drupal\Core\Language\LanguageManager $language_manager
   *   The language manager.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ConfigFactoryInterface $config_factory,
    AdminContext $admin_context,
    LanguageManager $language_manager,
    StateInterface $state
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
    $this->adminContext = $admin_context;
    $this->languageManager = $language_manager;
    $this->language = $this->languageManager->getCurrentLanguage()->getId();
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('router.admin_context'),
      $container->get('language_manager'),
      $container->get('state')
    );
  }

  /**
   * Get state configuration.
   *
   * @return array
   *   Return the state config.
   */
  private function getStateConfig() {
    $state_keys = [
      'gdpr_alert_active',
      'gdpr_alert.' . $this->language . '.title',
      'gdpr_alert.' . $this->language . '.dismiss_title',
      'gdpr_alert.' . $this->language . '.dismiss',
      'gdpr_alert.' . $this->language . '.message',
      'gdpr_alert_key',
    ];

    return $this->state->getMultiple($state_keys);
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    $config = $this->getStateConfig();
    if ($config && $config['gdpr_alert_active']) {

      if (!$this->adminContext->isAdminRoute()) {

        $alert = [
          'title' => !empty($config['gdpr_alert.' . $this->language . '.title']) ? $config['gdpr_alert.' . $this->language . '.title'] : '',
          'message' => [
            '#type' => 'processed_text',
            '#text' => $config['gdpr_alert.' . $this->language . '.message']['value'] ?? '',
            '#format' => $config['gdpr_alert.' . $this->language . '.message']['format'] ?? NULL,
          ],
          'dismiss' => $config['gdpr_alert.' . $this->language . '.dismiss'],
          'dismiss_title' => !empty($config['gdpr_alert.' . $this->language . '.dismiss_title']) ? $config['gdpr_alert.' . $this->language . '.dismiss_title'] : '',
        ];

        $build = [
          '#theme' => 'gdpr_alert',
          '#alert' => $alert,
        ];

        if (!empty($config['gdpr_alert_key'])) {

          $build['#attached'] = [
            'library' => ['gdpr_alert/alert_bar'],
            'drupalSettings' => [
              'gdpr_site_alert' => [
                'dismissedKey' => $config['gdpr_alert_key'],
              ],
            ],
          ];
        }

        $cacheableMetadata = new CacheableMetadata();
        $cacheableMetadata->addCacheableDependency($config);
        $cacheableMetadata->addCacheTags(['gdpr_alert']);
        $cacheableMetadata->applyTo($build);
      }
    }

    return $build;
  }

}
