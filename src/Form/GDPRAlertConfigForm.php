<?php

namespace Drupal\gdpr_alert\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheFactoryInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Language\LanguageManager;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Random;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * GDPR Alert Config Form.
 *
 * @package Drupal\gdpr_alert\Form
 */
class GDPRAlertConfigForm extends ConfigFormBase {

  /**
   * The cache.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The current language code.
   *
   * @var string
   */
  protected $language;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManager
   */
  protected $languageManager;

  /**
   * GDPRAlertConfigForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Cache\CacheFactoryInterface $cache_factory
   *   The cache factory.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state.
   * @param \Drupal\Core\Language\LanguageManager $language_manager
   *   The language manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    CacheFactoryInterface $cache_factory,
    StateInterface $state,
    LanguageManager $language_manager
  ) {
    parent::__construct($config_factory);
    $this->cache = $cache_factory->get('render');
    $this->state = $state;
    $this->language = $language_manager->getCurrentLanguage()->getId();
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('cache_factory'),
      $container->get('state'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'gdpr_alert_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['gdpr_alert.settings'];
  }

  /**
   * Get state configuration.
   *
   * @return array
   *   Return the state config.
   */
  private function getStateConfig() {
    $state_keys = [
      'gdpr_alert_active',
      'gdpr_alert.' . $this->language . '.title',
      'gdpr_alert.' . $this->language . '.dismiss_title',
      'gdpr_alert.' . $this->language . '.dismiss',
      'gdpr_alert.' . $this->language . '.message',
      'gdpr_alert_key',
    ];
    return $this->state->getMultiple($state_keys);
  }

  /**
   * Set state configuration data.
   *
   * @param array $data
   *   Set the state config.
   */
  private function setStateConfig(array $data) {
    $this->state->setMultiple($data);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $state_config = $this->getStateConfig();
    if (empty($state_config['gdpr_alert_active'])) {
      $this->messenger()->addWarning($this->t('GDPR alert is currently in-active. Please enable GDPR alert under "Settings" tab.'));
    }

    $form['alert'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('GDPR Alert (%language)', ['%language' => $this->languageManager->getCurrentLanguage()->getName()]),
    ];

    $form['alert']['alert_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Alert title'),
      '#default_value' => !empty($state_config['gdpr_alert.' . $this->language . '.title']) ? $state_config['gdpr_alert.' . $this->language . '.title'] : '',
    ];

    $message = !empty($state_config['gdpr_alert.' . $this->language . '.message']) ? $state_config['gdpr_alert.' . $this->language . '.message'] : [];
    $form['alert']['alert_message'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Alert message'),
      '#default_value' => !empty($message['value']) ? $message['value'] : '',
      '#required' => TRUE,
    ];

    if (empty($state_config['gdpr_alert.' . $this->language . '.dismiss'])) {
      $state_config['gdpr_alert.' . $this->language . '.dismiss'] = FALSE;
    }
    $form['alert']['alert_dismiss'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Make this alert dismissable?'),
      '#default_value' => $state_config['gdpr_alert.' . $this->language . '.dismiss'],
    ];

    $form['alert']['alert_dismiss_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Alert dismissal/agree button title'),
      '#default_value' => !empty($state_config['gdpr_alert.' . $this->language . '.dismiss_title']) ? $state_config['gdpr_alert.' . $this->language . '.dismiss_title'] : $this->t('I Agree'),
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save alert message'),
      '#name' => 'save',
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Save a random key so that we can use it to track a 'dismiss' action for
    // this particular alert.
    $random = new Random();

    // Set config.
    $this->setStateConfig([
      'gdpr_alert.' . $this->language . '.title' => $form_state->getValue('alert_title'),
      'gdpr_alert.' . $this->language . '.dismiss_title' => $form_state->getValue('alert_dismiss_title'),
      'gdpr_alert.' . $this->language . '.dismiss' => $form_state->getValue('alert_dismiss'),
      'gdpr_alert.' . $this->language . '.message' => $form_state->getValue('alert_message'),
      'gdpr_alert_key' => $random->string(16, TRUE),
    ]);

    // Invalidate cache tags.
    Cache::invalidateTags(['gdpr_alert']);
  }

}
