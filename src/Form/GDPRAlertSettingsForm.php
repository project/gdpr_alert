<?php

namespace Drupal\gdpr_alert\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheFactoryInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * GDPR Alert Settings Form.
 *
 * @package Drupal\gdpr_alert\Form
 */
class GDPRAlertSettingsForm extends ConfigFormBase {

  /**
   * The cache.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * GDPRAlertSettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Cache\CacheFactoryInterface $cache_factory
   *   The cache factory.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    CacheFactoryInterface $cache_factory,
    StateInterface $state
  ) {
    parent::__construct($config_factory);
    $this->cache = $cache_factory->get('render');
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('cache_factory'),
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'gdpr_alert_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['gdpr_alert.settings'];
  }

  /**
   * Get state configuration.
   *
   * @return array
   *   Return the state config.
   */
  private function getStateConfig() {
    $state_keys = [
      'gdpr_alert_active',
      'gdpr_alert_position',
      'gdpr_alert_expiration',
    ];
    return $this->state->getMultiple($state_keys);
  }

  /**
   * Set state configuration data.
   *
   * @param array $data
   *   Set the state config data.
   */
  private function setStateConfig(array $data) {
    $this->state->setMultiple($data);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $state_config = $this->getStateConfig();

    $form['alert'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Alert Settings'),
    ];

    $form['alert']['alert_active'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable GDPR alert'),
      '#description' => $this->t('Enable or disable site wide GDPR alert.'),
      '#default_value' => !empty($state_config['gdpr_alert_active']) ? $state_config['gdpr_alert_active'] : FALSE,
    ];

    $positions = [
      'top' => $this->t('Top'),
      'bottom' => $this->t('Bottom'),
    ];
    $form['alert']['alert_position'] = [
      '#type' => 'select',
      '#title' => $this->t('GDPR alert position'),
      '#options' => $positions,
      '#description' => $this->t('Set position of GDPR alert.'),
      '#default_value' => !empty($state_config['gdpr_alert_position']) ? $state_config['gdpr_alert_position'] : 'top',
    ];

    $expirations = [
      'year' => $this->t('1 Year'),
      'month' => $this->t('1 Month'),
      'week' => $this->t('1 Week'),
      'day' => $this->t('1 Day'),
      'default' => $this->t('Default'),
    ];
    $form['alert']['alert_expiration'] = [
      '#type' => 'select',
      '#title' => $this->t('GDPR alert cookie expiration'),
      '#options' => $expirations,
      '#description' => $this->t('Set expiration of the GDPR alert cookie.<br>The default value would set the cookie to expire when the browser session ends.'),
      '#default_value' => !empty($state_config['gdpr_alert_expiration']) ? $state_config['gdpr_alert_expiration'] : 'default',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Set config.
    $this->setStateConfig([
      'gdpr_alert_active' => $form_state->getValue('alert_active'),
      'gdpr_alert_position' => $form_state->getValue('alert_position'),
      'gdpr_alert_expiration' => $form_state->getValue('alert_expiration'),
    ]);

    // Invalidate cache tags.
    Cache::invalidateTags(['gdpr_alert']);
  }

}
