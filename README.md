# GDPR Alert

This module allows you to display a GDPR consent alert bar.

### Required

- Drupal 9.x+.

### Installation/Setup

1. Install and enable module.

2. Configure GDPR alert message/settings at `(admin/config/gdpr-alert)`.

3. Fill in required fields and save. Saving changes will flush render cache.
